package com.taxiapps.persiandate_utc;

import androidx.annotation.Nullable;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by SABIH on 9/25/2019.
 */
public class PersianDateUTCUtils {

    private static int oneDay = 24 * 60 * 60 * 1000;
    private static int oneHour = 60 * 60 * 1000;
    private static int oneMinute = 60 * 1000;

    public enum DateDiffFormat {
        Year_Month_Week_Day,
        Year_Month_Week_Day_Hour,
        Year_Month_Week_Day_Hour_Minute;
    }

    public enum AncoreMode {
        Before_va_After,
        Ago_va_Remaining;
    }

    public static DateDiff calculatePersianDiff(long toTimeStamp, boolean calcWeek, boolean calcHourAndMinute) {
        return calculatePersianDiff(null, toTimeStamp, calcWeek, calcHourAndMinute);
    }

    public static DateDiff calculatePersianDiff(Long fromTimeStamp, long toTimeStamp, boolean calcWeek, boolean calcHourAndMinute) {

        //create calender from time stamps
        PersianDateUTC startDate = fromTimeStamp == null ? new PersianDateUTC() : new PersianDateUTC(fromTimeStamp);
        PersianDateUTC toDate = new PersianDateUTC(toTimeStamp);

        //in this block we set hour minute second and millisecond to first of day
        //calc rage is for when we want calculate hours and minutes between too dates
        if (!calcHourAndMinute) {
            startDate = startDate.getStartOfDay();
            toDate = toDate.getStartOfDay();
        }

        //calculate remain of time stamps subtraction
        long daysTsDiff = toDate.getTime() - startDate.getTime();

        //calculate if toTimeStamp is before fromTimeStamp or not
        boolean isPast = daysTsDiff < 0;

        //turn daysTsDiff to day
        long daysDiff = Math.abs(daysTsDiff / oneDay);
        long hoursAndMinutes = Math.abs(daysTsDiff) - (daysDiff * oneDay);

        //choose what calender should be start calender for progress
        PersianDateUTC start = isPast ? toDate : startDate;

        long years = 0;
        long months = 0;
        long weeks = 0;
        long days = 0;
        long hour = hoursAndMinutes / oneHour;
        long minute = (hoursAndMinutes - (hour * oneHour)) / oneMinute;

        //this block calculate year
        if (daysDiff >= getDaysToYearsForward(start, 1)) {
            for (int i = 2; ; i++) {
                if (daysDiff < getDaysToYearsForward(start, i)) {
                    years = i - 1;
                    daysDiff -= getDaysToYearsForward(start, (int) years);
                    break;
                }
            }
        }

        //this block calculate months
        if (daysDiff >= getDaysToMonthsForward(start, 1)) {
            for (int i = 2; ; i++) {
                if (daysDiff < getDaysToMonthsForward(start, i)) {
                    months = i - 1;
                    daysDiff -= getDaysToMonthsForward(start, (int) months);
                    break;
                }
            }
        }

        //this block calculate week (if flag is true)
        if (calcWeek) {
            if (daysDiff >= 7) {
                for (int i = 2; ; i++) {
                    if (daysDiff < i * 7) {
                        weeks = i - 1;
                        daysDiff -= weeks * 7;
                        break;
                    }
                }
            }
        }

        //this block calculate days
        if (daysDiff >= 1) {
            for (int i = 2; ; i++) {
                if (daysDiff < i) {
                    days = i - 1;
                    break;
                }
            }
        }

        return new DateDiff(years, months, weeks, days, hour, minute, isPast);
    }

    public static DateDiff calculatePersianDiffInDays(Long fromTimeStamp, long toTimeStamp, boolean calcHourAndMinute) {
        fromTimeStamp = fromTimeStamp == null ? System.currentTimeMillis() : fromTimeStamp;

        //calculate remain of time stamps subtraction
        boolean fromTsHasDayLight = TimeZone.getDefault().inDaylightTime(new Date(fromTimeStamp));
        boolean toTsHasDayLight = TimeZone.getDefault().inDaylightTime(new Date(toTimeStamp));

        if (fromTsHasDayLight) {
            fromTimeStamp += TimeZone.getDefault().getDSTSavings();
        }
        if (toTsHasDayLight) {
            toTimeStamp += TimeZone.getDefault().getDSTSavings();
        }

        long daysTsDiff = toTimeStamp - fromTimeStamp;

        //calculate if toTimeStamp is before fromTimeStamp or not
        boolean isPast = daysTsDiff < 0;

        long tsDifference = Math.abs(daysTsDiff);
        long days = TimeUnit.MILLISECONDS.toDays(tsDifference);
        long hours = 0;
        long minutes = 0;

        if (calcHourAndMinute) {
            hours = TimeUnit.MILLISECONDS.toHours(tsDifference) - TimeUnit.DAYS.toHours(days);
            minutes = TimeUnit.MILLISECONDS.toMinutes(tsDifference)
                    - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(tsDifference));
        }

        return new DateDiff(0, 0, 0, days, hours, minutes, isPast);
    }

    private static long getDaysToYearsForward(PersianDateUTC startDate, int years) {
        PersianDateUTC date = new PersianDateUTC(startDate.getYear() + years, startDate.getMonth(), startDate.getDay(), 0, 0, 0, 0);

        long[] diffArr = startDate.untilToday(date);

        if (diffArr[1] > 12) {
            diffArr[0]++;
        }

        return diffArr[0];
    }

    private static long getDaysToMonthsForward(PersianDateUTC startDate, int month) {
        int _day = startDate.getDay();
        int _month = startDate.getMonth();
        int _year = startDate.getYear();

        _month += month;
        _year += (_month - 1) / 12;
        _month = _month % 12;
        _month = _month == 0 ? 12 : _month;

        PersianDateUTC date = new PersianDateUTC(_year, _month, _day, 0, 0, 0, 0);

        long[] diffArr = startDate.untilToday(date);

        if (diffArr[1] > 12) {
            diffArr[0]++;
        }

        return diffArr[0];
    }

    public static class DateDiff {
        private long year;
        private long month;
        private long week;
        private long day;
        private long hour;
        private long minute;
        private boolean isPast;
        private boolean justDuration;
        private DateDiffFormat dateDiffFormat;
        private AncoreMode ancoreMode;

        public DateDiff(long year, long month, long week, long day, long hour, long minute, boolean isPast) {
            this.year = year;
            this.month = month;
            this.week = week;
            this.day = day;
            this.hour = hour;
            this.minute = minute;
            this.isPast = isPast;
        }

        public String format(@Nullable Locale locale, DateDiffFormat dateDiffFormat, AncoreMode ancoreMode, boolean justDuration) {
            this.justDuration = justDuration;
            this.dateDiffFormat = dateDiffFormat;
            this.ancoreMode = ancoreMode;
            if (locale == null) {
                locale = Locale.getDefault();
            }

            switch (locale.getLanguage()) {
                case "fa":
                    return persianFormat();
                case "tr":
                    return turkishFormat();
                case "ar":
                    return arabicFormat();
                case "en":
                default:
                    return englishFormat();
            }

        }

        private String persianFormat() {
            String res = "";

            if (!justDuration && year == 0 && month == 0 && week == 0 && day == 0 && hour == 0 && minute == 0) {
                return "امروز";
            } else if (!justDuration && year == 0 && month == 0 && week == 0 && day == 1 && hour == 0 && minute == 0) {
                return isPast ? "دیروز" : "فردا";
            } else {
                if (year > 0) {
                    res += (year + " سال و ");
                } else {
                    res += "";
                }

                if (month > 0) {
                    res += (month + " ماه و ");
                } else {
                    res += "";
                }

                if (week > 0) {
                    res += (week + " هفته و ");
                } else {
                    res += "";
                }

                if (day > 0) {
                    res += (day + " روز و ");
                } else {
                    res += "";
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour ||
                        dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (hour > 0) {
                        res += (hour + " ساعت و ");
                    } else {
                        res += "";
                    }
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (minute > 0) {
                        res += (minute + " دقیقه و ");
                    } else {
                        res += "";
                    }
                }

                if (res.length() > 0) {
                    res = res.substring(0, res.lastIndexOf(" و "));
                    switch (ancoreMode) {
                        case Ago_va_Remaining:
                            res += (isPast ? " گذشته" : " مانده");
                            break;
                        case Before_va_After:
                        default:
                            res += (isPast ? " بعد" : " قبل");
                            break;
                    }
                } else {
                    res += "0";
                    res += " روز";
                }

                return toPersianDigit(res);
            }
        }

        private String englishFormat() {
            String res = "";

            if (!justDuration && year == 0 && month == 0 && week == 0 && day == 0 && hour == 0 && minute == 0) {
                return "Today";
            } else if (!justDuration && year == 0 && month == 0 && week == 0 && day == 1 && hour == 0 && minute == 0) {
                return isPast ? "Yesterday" : "Tomorrow";
            } else {
                if (year > 0) {
                    res += (year + (year > 1 ? " years and " : " year and "));
                } else {
                    res += "";
                }

                if (month > 0) {
                    res += (month + (month > 1 ? " months and " : " month and "));
                } else {
                    res += "";
                }

                if (week > 0) {
                    res += (week + (week > 1 ? " weeks and " : " week and "));
                } else {
                    res += "";
                }

                if (day > 0) {
                    res += (day + (day > 1 ? " days and " : " day and "));
                } else {
                    res += "";
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour ||
                        dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (hour > 0) {
                        res += (hour + (hour > 1 ? " hours and " : " hour and "));
                    } else {
                        res += "";
                    }
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (minute > 0) {
                        res += (minute + (minute > 1 ? " minutes and " : " minute and "));
                    } else {
                        res += "";
                    }
                }

                if (res.length() > 0) {
                    res = res.substring(0, res.lastIndexOf(" and "));
                    switch (ancoreMode) {
                        case Ago_va_Remaining:
                            res += (isPast ? " ago" : " remaining");
                            break;
                        case Before_va_After:
                        default:
                            res += (isPast ? " after" : " before");
                            break;
                    }
                } else {
                    res += "0";
                    res += " day";
                }

                return toEnglishDigit(res);
            }
        }

        private String turkishFormat() {
            String res = "";

            if (!justDuration && year == 0 && month == 0 && week == 0 && day == 0 && hour == 0 && minute == 0) {
                return "bugün";
            } else if (!justDuration && year == 0 && month == 0 && week == 0 && day == 1 && hour == 0 && minute == 0) {
                return isPast ? "dün" : "yarın";
            } else {
                if (year > 0) {
                    res += (year + (year > 1 ? " yıllar ve " : " yıl ve "));
                } else {
                    res += "";
                }

                if (month > 0) {
                    res += (month + (month > 1 ? " aylar ve " : " ay ve "));
                } else {
                    res += "";
                }

                if (week > 0) {
                    res += (week + (week > 1 ? " haftalar ve " : " hafta ve "));
                } else {
                    res += "";
                }

                if (day > 0) {
                    res += (day + (day > 1 ? " günler ve " : " gün ve "));
                } else {
                    res += "";
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour ||
                        dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (hour > 0) {
                        res += (hour + (hour > 1 ? " saatler ve " : " saat ve "));
                    } else {
                        res += "";
                    }
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (minute > 0) {
                        res += (minute + (minute > 1 ? " dakika ve " : " dakika ve "));
                    } else {
                        res += "";
                    }
                }

                if (res.length() > 0) {
                    res = res.substring(0, res.lastIndexOf(" ve "));
                    switch (ancoreMode) {
                        case Ago_va_Remaining:
                            res += (isPast ? " önce" : " kalan");
                            break;
                        case Before_va_After:
                        default:
                            res += (isPast ? " sonra" : " önce");
                            break;
                    }
                } else {
                    res += "0";
                    res += " gün";
                }

                return toEnglishDigit(res);
            }
        }

        private String arabicFormat() {
            String res = "";

            if (!justDuration && year == 0 && month == 0 && week == 0 && day == 0 && hour == 0 && minute == 0) {
                return "اليوم";
            } else if (!justDuration && year == 0 && month == 0 && week == 0 && day == 1 && hour == 0 && minute == 0) {
                return isPast ? "في الامس" : "غدا";
            } else {
                if (year > 0) {
                    if (year == 1) {
                        res += (year + " سنة و ");
                    } else if (year == 2) {
                        res += (year + " سنة و ");
                    } else if (year <= 10) {
                        res += (year + " سنوات و ");
                    } else {
                        res += (year + " سنة و ");
                    }
                } else {
                    res += "";
                }

                if (month > 0) {
                    if (month == 1) {
                        res += (month + " شهر و ");
                    } else if (month == 2) {
                        res += (month + " شهران و ");
                    } else if (month <= 10) {
                        res += (month + " شهور و ");
                    } else {
                        res += (month + " شهر و ");
                    }
                } else {
                    res += "";
                }

                if (week > 0) {
                    if (week == 1) {
                        res += (week + " أسبوع و ");
                    } else if (week == 2) {
                        res += (week + " أسابيع و ");
                    } else if (week <= 10) {
                        res += (week + " أسابيع و ");
                    } else {
                        res += (week + " أسبوع و ");
                    }
                } else {
                    res += "";
                }

                if (day > 0) {
                    if (day == 1) {
                        res += (" يوم واحد و ");
                    } else if (day == 2) {
                        res += (day + " يوم و ");
                    } else if (day <= 10) {
                        res += (day + " يوم و ");
                    } else {
                        res += (day + " يوم و ");
                    }
                } else {
                    res += "";
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour ||
                        dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (hour > 0) {
                        if (hour == 1) {
                            res += (hour + " ساعة و ");
                        } else if (hour == 2) {
                            res += (hour + " ساعتين و ");
                        } else if (hour <= 10) {
                            res += (hour + " ساعات و ");
                        } else {
                            res += (hour + " ساعة و ");
                        }
                    } else {
                        res += "";
                    }
                }

                if (dateDiffFormat == DateDiffFormat.Year_Month_Week_Day_Hour_Minute) {
                    if (minute > 0) {
                        if (minute == 1) {
                            res += (minute + " دقيقة و ");
                        } else if (minute == 2) {
                            res += (minute + " دقيقة و ");
                        } else if (minute <= 10) {
                            res += (minute + " دقائق و ");
                        } else {
                            res += (minute + " دقيقة و ");
                        }
                    } else {
                        res += "";
                    }
                }

                if (res.length() > 0) {
                    res = res.substring(0, res.lastIndexOf(" و "));
                    switch (ancoreMode) {
                        case Ago_va_Remaining:
                            res += (isPast ? " منذ" : " المتبقي");
                            break;
                        case Before_va_After:
                        default:
                            res += (isPast ? " بعد" : " قبل");
                            break;
                    }
                } else {
                    res += "۰";
                    res += " يوم";
                }

                return toArabicDigit(res);
            }
        }
    }

    private static String toEnglishDigit(String persianDigit) {
        return persianDigit.replace("۰", "0")
                .replace("۱", "1")
                .replace("۲", "2")
                .replace("۳", "3")
                .replace("۴", "4")
                .replace("۵", "5")
                .replace("۶", "6")
                .replace("۷", "7")
                .replace("۸", "8")
                .replace("۹", "9");
    }

    private static String toPersianDigit(String englishDigit) {
        return englishDigit.replace("0", "۰")
                .replace("1", "۱")
                .replace("2", "۲")
                .replace("3", "۳")
                .replace("4", "۴")
                .replace("5", "۵")
                .replace("6", "۶")
                .replace("7", "۷")
                .replace("8", "۸")
                .replace("9", "۹");
    }

    private static String toArabicDigit(String anyDigit) {
        return anyDigit.replace("0", "٠")
                .replace("۰", "٠")
                .replace("۱", "١")
                .replace("1", "١")
                .replace("۲", "٢")
                .replace("2", "٢")
                .replace("۳", "٣")
                .replace("3", "٣")
                .replace("۴", "٤")
                .replace("4", "٤")
                .replace("۵", "٥")
                .replace("5", "٥")
                .replace("۶", "٦")
                .replace("6", "٦")
                .replace("۷", "٧")
                .replace("7", "٧")
                .replace("۸", "٨")
                .replace("8", "٨")
                .replace("۹", "٩")
                .replace("9", "٩");
    }
}
