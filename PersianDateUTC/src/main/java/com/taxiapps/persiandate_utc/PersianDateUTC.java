package com.taxiapps.persiandate_utc;

import android.util.Pair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class PersianDateUTC {
    /*----- Define Variable ---*/
    private Long timeInMilliSecond;
    public static final int FARVARDIN = 1;
    public static final int ORDIBEHEST = 2;
    public static final int KHORDAD = 3;
    public static final int TIR = 4;
    public static final int MORDAD = 5;
    public static final int SHAHRIVAR = 6;
    public static final int MEHR = 7;
    public static final int ABAN = 8;
    public static final int AZAR = 9;
    public static final int DAY = 10;
    public static final int BAHMAN = 11;
    public static final int ESFAND = 12;
    public static final int AM = 1;
    public static final int PM = 2;
    public static final String AM_SHORT_NAME = "ق.ظ";
    public static final String PM_SHORT_NAME = "ب.ظ";
    public static final String AM_NAME = "قبل از ظهر";
    public static final String PM_NAME = "بعد از ظهر";
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;
    private int millisecond;

    /**
     * Constructor
     */
    public PersianDateUTC(int year, int month, int day) {
        this.year = year;
        this.month = validateMonth(month);
        this.day = validateDay(day);
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
        this.millisecond = 0;
        prepareDate();
        this.changeTime();
    }

    /**
     * Constructor
     */
    public PersianDateUTC(int year, int month, int day, int hour, int minute, int second, int millisecond) {
        this.year = year;
        this.month = validateMonth(month);
        this.day = validateDay(day);
        this.hour = validateHour(hour);
        this.minute = validateMinuteAndSecond(minute);
        this.second = validateMinuteAndSecond(second);
        this.millisecond = validateMillisecond(millisecond);
        prepareDate();
        this.changeTime();
    }

    /**
     * Constructor
     */
    public PersianDateUTC() {
        this.timeInMilliSecond = System.currentTimeMillis();
        this.changeTime();
    }

    /**
     * Constructor
     */
    public PersianDateUTC(Long timeInMilliSecond) {
        this.timeInMilliSecond = timeInMilliSecond;
        this.changeTime();
    }

    /**
     * Constructor
     */
    public PersianDateUTC(Date date) {
        this.timeInMilliSecond = date.getTime();
        this.changeTime();
    }

    /**
     * ---- Dont change---
     */
    private final int[][] grgSumOfDays = {{0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365}, {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}};
    private final int[][] hshSumOfDays = {{0, 31, 62, 93, 124, 155, 186, 216, 246, 276, 306, 336, 365}, {0, 31, 62, 93, 124, 155, 186, 216, 246, 276, 306, 336, 366}};
    private final String[] dayNames = {"شنبه", "یک‌شنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه"};
    private final String[] monthNames = {"فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"};
    private final String[] AfghanMonthNames = {"حمل", "ثور", "جوزا", "سرطان", "اسد", "سنبله", "میزان", "عقرب", "قوس", "جدی", "دلو", "حوت"};
    private final String[] KurdishMonthNames = {"جیژنان", "گولان", "زه ردان", "په رپه ر", "گه لاویژ", "نوخشان", "به ران", "خه زان", "ساران", "بفران", "به ندان", "رمشان"};
    private final String[] PashtoMonthNames = {"وری", "غويی", "غبرګولی", "چنګاښ", "زمری", "وږی", "تله", "لړم", "ليندۍ", "مرغومی", "سلواغه", "كب"};

    /*---- Setter And getter ----*/
    public int getYear() {
        return year;
    }

    public PersianDateUTC setYear(int year) {
        this.year = year;
        this.day = validateDay(day);
        prepareDate();
        return this;
    }

    public int getMonth() {
        return month;
    }

    public PersianDateUTC setMonth(int month) {
        this.month = validateMonth(month);
        this.day = validateDay(this.day);
        prepareDate();
        return this;
    }

    public int getDay() {
        return day;
    }

    public PersianDateUTC setDay(int day) {
        this.day = validateDay(day);
        this.prepareDate();
        return this;
    }

    public int getHour() {
        return hour;
    }

    public PersianDateUTC setHour(int hour) {
        this.hour = validateHour(hour);
        prepareDate();
        return this;
    }

    public int getMinute() {
        return minute;
    }

    public PersianDateUTC setMinute(int minute) {
        this.minute = validateMinuteAndSecond(minute);
        prepareDate();
        return this;
    }

    public int getSecond() {
        return second;
    }

    public PersianDateUTC setSecond(int second) {
        this.second = validateMinuteAndSecond(second);
        prepareDate();
        return this;
    }

    public int getMillisecond() {
        return millisecond;
    }

    public PersianDateUTC setMillisecond(int millisecond) {
        this.millisecond = validateMillisecond(millisecond);
        prepareDate();
        return this;
    }

    /**
     * init without time
     *
     * @param grgYear  Yera in Grg
     * @param grgMonth Month in Grg
     * @param grgDay   Day in Grg
     * @return modified PersianDate
     */
    public PersianDateUTC initDateByGrg(int grgYear, int grgMonth, int grgDay) {
        return this.initDateByGrg(grgYear, grgMonth, grgDay, 0, 0, 0, 0);
    }

    /**
     * init with Grg data
     *
     * @param grgYear     Year in Grg
     * @param grgMonth    Month in Grg
     * @param grgDay      day in Grg
     * @param hour        hour
     * @param minute      min
     * @param second      second
     * @param millisecond millisecond
     * @return modified PersianDate
     */
    public PersianDateUTC initDateByGrg(int grgYear, int grgMonth, int grgDay, int hour, int minute, int second, int millisecond) {
        int[] convert = this.toJalali(grgYear, grgMonth, grgDay);
        this.year = convert[0];
        this.month = validateMonth(convert[1]);
        this.day = validateDay(convert[2]);
        this.hour = validateHour(hour);
        this.minute = validateMinuteAndSecond(minute);
        this.second = validateMinuteAndSecond(second);
        this.millisecond = validateMillisecond(millisecond);
        prepareDate();
        return this;
    }

    /**
     * initilize date from jallai date
     *
     * @param shYear  Year in jallali date
     * @param shMonth Month in Jallali date
     * @param shDay   daye in Jalalli date
     * @return modified PersianDate
     */
    public PersianDateUTC initDateByJalali(int shYear, int shMonth, int shDay) {
        return this.initDateByJalali(shYear, shMonth, shDay, 0, 0, 0, 0);
    }

    /**
     * initilize date from jallai date
     *
     * @param shYear      Year in jallali date
     * @param shMonth     Month in Jallali date
     * @param shDay       daye in Jalalli date
     * @param hour        Hour
     * @param minute      Minute
     * @param second      Second
     * @param millisecond Millisecond
     * @return modified PersianDate
     */
    public PersianDateUTC initDateByJalali(int shYear, int shMonth, int shDay, int hour, int minute, int second, int millisecond) {
        this.year = shYear;
        this.month = validateMonth(shMonth);
        this.day = validateDay(shDay);
        this.hour = validateHour(hour);
        this.minute = validateMinuteAndSecond(minute);
        this.second = validateMinuteAndSecond(second);
        this.millisecond = validateMillisecond(millisecond);
        prepareDate();
        return this;
    }

    /**
     * init with timestamp
     *
     * @param timeInMilliSecond timeInMillis
     * @return modified PersianDate
     */
    public PersianDateUTC initDate(long timeInMilliSecond) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(timeInMilliSecond);
        int[] convert = this.toJalali(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        this.year = convert[0];
        this.month = validateMonth(convert[1]);
        this.day = validateDay(convert[2]);
        this.hour = validateHour(calendar.get(Calendar.HOUR_OF_DAY));
        this.minute = validateMinuteAndSecond(calendar.get(Calendar.MINUTE));
        this.second = validateMinuteAndSecond(calendar.get(Calendar.SECOND));
        this.millisecond = validateMillisecond(calendar.get(Calendar.MILLISECOND));
        this.timeInMilliSecond = timeInMilliSecond;
        return this;
    }

    /**
     * Helper function for inilize
     */
    private void prepareDate() {
        int[] toGrg = //TODO REVIEW
                this.getYear() > 1360 ?
                        this.toGregorian(this.getYear(), this.getMonth(), this.getDay()) :
                        this.toGregorianBefore1360(this.getYear(), this.getMonth(), this.getDay());
        String dtStart = "" + this.textNumberFilter(String.valueOf(toGrg[0])) + "-" + this.textNumberFilter(String.valueOf(toGrg[1])) + "-" + this.textNumberFilter(String.valueOf(toGrg[2]))
                + "T" + this.textNumberFilter(String.valueOf(this.getHour())) + ":" + this.textNumberFilter(String.valueOf(this.getMinute())) + ":" + this.textNumberFilter(String.valueOf(this.getSecond()))
                + "." + this.millisecondNumberFilter(String.valueOf(this.getMillisecond())) + "Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            this.timeInMilliSecond = format.parse(dtStart).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * return time in long value
     *
     * @return Value of time in mile
     */
    public Long getTime() {
        return this.timeInMilliSecond;
    }

    /**
     * set time in milliseconds
     *
     * @return instance of persian date
     */
    public PersianDateUTC setTimeInMillis(Long timeInMilliSecond) {
        this.timeInMilliSecond = timeInMilliSecond;
        this.changeTime();
        return this;
    }

    /**
     * Check Grg year is leap
     *
     * @param year year to find is leap
     * @return grg is leap year
     */
    public boolean grgIsLeap(int year) {
        return ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
    }

    /**
     * Check year in Leap
     *
     * @return true or false
     */
    public boolean isLeap() {
        return this.isLeap(this.year);
    }

    /**
     * Check custom year is leap
     *
     * @param year int year
     * @return true or false
     */
    public boolean isLeap(int year) {
        double referenceYear = 1375;
        double startYear = 1375;
        double yearRes = year - referenceYear;
        if (yearRes > 0) {
            if (yearRes >= 33) {
                double numb = yearRes / 33;
                startYear = referenceYear + Math.floor(numb) * 33;
            }
        } else {
            if (yearRes >= -33) {
                startYear = referenceYear - 33;
            } else {
                double numb = Math.abs(yearRes / 33);
                startYear = referenceYear - (Math.floor(numb) + 1) * 33;
            }
        }
        double[] leapYears = {startYear, startYear + 4, startYear + 8, startYear + 12, startYear + 16, startYear + 20, startYear + 24, startYear + 28, startYear + 33};
        return (Arrays.binarySearch(leapYears, year)) >= 0;
    }

    /**
     * Check static is leap year for Jalali Date
     *
     * @param year Jalali year
     * @return true if year is leap
     */
    public static boolean isJalaliLeap(int year) {
        return (new PersianDateUTC().isLeap(year));
    }

    /**
     * Check static is leap year for Grg Date
     *
     * @param year year to find is leap
     * @return grg is leap year
     */
    public static boolean isGrgLeap(int year) {
        return (new PersianDateUTC().grgIsLeap(year));
    }

    /**
     * Convert Grg date to jalali date
     *
     * @param year  year in Grg date
     * @param month month in Grg date
     * @param day   day in Grg date
     * @return a int[year][month][day] in jalali date
     */
    public int[] toJalali(int year, int month, int day) {
        int hshDay = 0;
        int hshMonth = 0;
        int hshElapsed;
        int hshYear = year - 621;
        boolean grgLeap = this.grgIsLeap(year);
        boolean hshLeap = this.isLeap(hshYear - 1);
        int grgElapsed = grgSumOfDays[(grgLeap ? 1 : 0)][month - 1] + day;
        int XmasToNorooz = (hshLeap && grgLeap) ? 80 : 79;
        if (grgElapsed <= XmasToNorooz) {
            hshElapsed = grgElapsed + 286;
            hshYear--;
            if (hshLeap && !grgLeap)
                hshElapsed++;
        } else {
            hshElapsed = grgElapsed - XmasToNorooz;
            hshLeap = this.isLeap(hshYear);
        }
        if (year >= 2029 && (year - 2029) % 4 == 0) {
            hshElapsed++;
        }
        for (int i = 1; i <= 12; i++) {
            if (hshSumOfDays[(hshLeap ? 1 : 0)][i] >= hshElapsed) {
                hshMonth = i;
                hshDay = hshElapsed - hshSumOfDays[(hshLeap ? 1 : 0)][i - 1];
                break;
            }
        }
        if (hshMonth == 0 && hshDay == 0) {// TOF MALI
            hshYear = hshYear + 1;
            hshMonth = hshMonth + 1;
            hshDay = hshDay + 1;
        }
        int[] ret = {hshYear, hshMonth, hshDay};
        return ret;
    }

    /**
     * Convert Jalali date to Grg
     *
     * @param year  Year in jalali
     * @param month Month in Jalali
     * @param day   Day in Jalali
     * @return int[year][month][day]
     */
    public int[]
    toGregorian(int year, int month, int day) {
        int grgYear = year + 621;
        int grgDay = 0;
        int grgMonth = 0;
        int grgElapsed;

        boolean hshLeap = this.isLeap(year);
        boolean grgLeap = this.grgIsLeap(grgYear);

        int hshElapsed = hshSumOfDays[hshLeap ? 1 : 0][month - 1] + day;

        if (month > 10 || (month == 10 && hshElapsed > 286 + (grgLeap ? 1 : 0))) {
            grgElapsed = hshElapsed - (286 + (grgLeap ? 1 : 0));
            grgLeap = grgIsLeap(++grgYear);
        } else {
            hshLeap = this.isLeap(year - 1);
            grgElapsed = hshElapsed + 79 + (hshLeap ? 1 : 0) - (grgIsLeap(grgYear - 1) ? 1 : 0);
        }
        if (grgYear >= 2030 && (grgYear - 2030) % 4 == 0) {
            grgElapsed--;
        }
        if (grgYear == 1989) {
            grgElapsed++;
        }
        for (int i = 1; i <= 12; i++) {
            if (grgSumOfDays[grgLeap ? 1 : 0][i] >= grgElapsed) {
                grgMonth = i;
                grgDay = grgElapsed - grgSumOfDays[grgLeap ? 1 : 0][i - 1];
                break;
            }
        }
        return new int[]{grgYear, grgMonth, grgDay};
    }

    // TODO REVIEW
    private int[] toGregorianBefore1360(int year, int month, int day) {
        year += 1595;
        int[] out = {
                0,
                0,
                -355668 + (365 * year) + (((int) (year / 33)) * 8) + ((int) (((year % 33) + 3) / 4)) + day + ((month < 7) ? (month - 1) * 31 : ((month - 7) * 30) + 186)
        };
        out[0] = 400 * ((int) (out[2] / 146097));
        out[2] %= 146097;
        if (out[2] > 36524) {
            out[0] += 100 * ((int) (--out[2] / 36524));
            out[2] %= 36524;
            if (out[2] >= 365) out[2]++;
        }
        out[0] += 4 * ((int) (out[2] / 1461));
        out[2] %= 1461;
        if (out[2] > 365) {
            out[0] += (int) ((out[2] - 1) / 365);
            out[2] = (out[2] - 1) % 365;
        }
        int[] sal_a = {0, 31, ((out[0] % 4 == 0 && out[0] % 100 != 0) || (out[0] % 400 == 0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        for (out[2]++; out[1] < 13 && out[2] > sal_a[out[1]]; out[1]++) out[2] -= sal_a[out[1]];
        return out;
    }


    /**
     * calc day of week
     *
     * @return int
     */
    public int dayOfWeek() {
        return this.dayOfWeek(this);
    }

    /**
     * Get day of week from PersianDate object
     *
     * @param date persianDate
     * @return int
     */
    public int dayOfWeek(PersianDateUTC date) {
        return this.dayOfWeek(date.toDate());
    }

    /**
     * Get day of week from Date object
     *
     * @param date to find day of week
     * @return day of week index
     */
    public int dayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) return 0;
        return (cal.get(Calendar.DAY_OF_WEEK));
    }

    /**
     * return month name
     *
     * @return month name
     */
    public String monthName() {
        return this.monthName(this.getMonth());
    }

    /**
     * Return month name
     *
     * @param month ShMonth
     * @return month name
     */
    public String monthName(int month) {
        return this.monthNames[month - 1];
    }

    /**
     * Get month name in Afghan
     *
     * @param month month
     * @return month name in Afghan
     */
    public String AfghanMonthName(int month) {
        return this.AfghanMonthNames[month - 1];
    }

    /**
     * Get current date Afghan month name
     *
     * @return current date Afghan month name
     */
    public String AfghanMonthName() {
        return this.AfghanMonthName(this.getMonth());
    }

    /**
     * Get month name in Kurdish
     *
     * @param month month
     * @return month name in Kurdish
     */
    public String KurdishMonthName(int month) {
        return this.KurdishMonthNames[month - 1];
    }

    /**
     * Get current date Kurdish month name
     *
     * @return current date Kurdish month name
     */
    public String KurdishMonthName() {
        return this.KurdishMonthName(this.getMonth());
    }

    /**
     * Get month name in Pashto
     *
     * @param month month
     * @return month name in Pashto
     */
    public String PashtoMonthName(int month) {
        return this.PashtoMonthNames[month - 1];
    }

    /**
     * Get current date Pashto month name
     *
     * @return current date Pashto month name
     */
    public String PashtoMonthName() {
        return this.PashtoMonthName(this.getMonth());
    }

    /**
     * Get Day Name
     *
     * @return day name
     */
    public String dayName() {
        return this.dayName(this);
    }

    /**
     * Get Day Name
     *
     * @param date date
     * @return day name
     */
    public String dayName(PersianDateUTC date) {
        return this.dayNames[this.dayOfWeek(date)];
    }

    /**
     * Number days of month
     *
     * @return return days
     */
    public int getMonthDays() {
        return this.getMonthDays(this.getYear(), this.getMonth());
    }

    /**
     * calc count of day in month
     *
     * @param year  year
     * @param month month
     * @return count of day in month
     */
    public int getMonthDays(int year, int month) {
        if (month == 12 && !this.isLeap(year)) {
            return 29;
        }
        if (month <= 6) {
            return 31;
        } else {
            return 30;
        }
    }

    /**
     * calculate day in year
     *
     * @return day in year
     */
    public int getDayInYear() {
        return this.getDayInYear(this.getMonth(), getDay());
    }

    /**
     * Calc day of the year
     *
     * @param month Month
     * @param day   Day
     * @return day of the year
     */
    public int getDayInYear(int month, int day) {
        for (int i = 1; i < month; i++) {
            if (i <= 6) {
                day += 31;
            } else {
                day += 30;
            }
        }
        return day;
    }

    /**
     * Add year
     *
     * @param year Years to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addYear(int year) {
        return this.initDateByJalali(this.getYear() + year, this.getMonth(), this.getDay(), this.getHour(), this.getMinute(), this.getSecond(), this.getMillisecond());
    }

    /**
     * Add year
     *
     * @param year        Years to Add (can be negative)
     * @param currentDate date to which years can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addYear(int year, PersianDateUTC currentDate) {
        return currentDate.addYear(year);
    }

    /**
     * Add Month
     *
     * @param month Months to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addMonth(int month) {
        int resYear = this.getYear() + (month / 12);
        int resMonth = this.getMonth() + (month % 12);
        if (resMonth < 1) {
            resYear--;
            resMonth = 12 - Math.abs(resMonth);
        } else if (resMonth > 12) {
            resYear++;
            resMonth = resMonth % 12;
        }
        return this.initDateByJalali(resYear, resMonth, this.getDay(), this.getHour(), this.getMinute(), this.getSecond(), this.getMillisecond());
    }

    /**
     * Add Month
     *
     * @param month       Months to Add (can be negative)
     * @param currentDate date to which months can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addMonth(int month, PersianDateUTC currentDate) {
        return currentDate.addMonth(month);
    }

    /**
     * Add Week
     *
     * @param week Weeks to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addWeek(int week) {
        return this.addDay(week * 7);
    }

    /**
     * Add Week
     *
     * @param week        Weeks to Add (can be negative)
     * @param currentDate date to which weeks can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addWeek(int week, PersianDateUTC currentDate) {
        return currentDate.addWeek(week);
    }

    /**
     * Add Day
     *
     * @param day Days to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addDay(int day) {
        long daysInMillis = (long) day * 86400000;
        long res = this.timeInMilliSecond + daysInMillis;
        boolean fromTsHasDayLight = TimeZone.getDefault().inDaylightTime(new Date(this.timeInMilliSecond));
        boolean toTsHasDayLight = TimeZone.getDefault().inDaylightTime(new Date(res));
        if (fromTsHasDayLight && !toTsHasDayLight) {
            res += TimeZone.getDefault().getDSTSavings();
        } else if (!fromTsHasDayLight && toTsHasDayLight) {
            res -= TimeZone.getDefault().getDSTSavings();
        }
        return initDate(res);
    }

    /**
     * Add Day
     *
     * @param day         Days to Add (can be negative)
     * @param currentDate date to which days can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addDay(int day, PersianDateUTC currentDate) {
        return currentDate.addDay(day);
    }

    /**
     * Add Hour
     *
     * @param hour Hours to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addHour(int hour) {
        return this.addMinute(hour * 60);
    }

    /**
     * Add Hour
     *
     * @param hour        Hours to Add (can be negative)
     * @param currentDate date to which days can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addHour(int hour, PersianDateUTC currentDate) {
        return currentDate.addHour(hour);
    }

    /**
     * Add Minute
     *
     * @param minute Minutes to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addMinute(int minute) {
        return this.addSecond(minute * 60);
    }

    /**
     * Add Minute
     *
     * @param minute      Minutes to Add (can be negative)
     * @param currentDate date to which days can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addMinute(int minute, PersianDateUTC currentDate) {
        return currentDate.addMinute(minute);
    }

    /**
     * Add Second
     *
     * @param second Seconds to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addSecond(int second) {
        return this.addMillisecond(second * 1_000);
    }

    /**
     * Add Second
     *
     * @param second      Seconds to Add (can be negative)
     * @param currentDate date to which days can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addSecond(int second, PersianDateUTC currentDate) {
        return currentDate.addSecond(second);
    }

    /**
     * Add Millisecond
     *
     * @param millisecond Milliseconds to Add (can be negative)
     * @return modified PersianDate
     */
    public PersianDateUTC addMillisecond(int millisecond) {
        long res = this.timeInMilliSecond + millisecond;
        boolean fromTsHasDayLight = TimeZone.getDefault().inDaylightTime(new Date(this.timeInMilliSecond));
        boolean toTsHasDayLight = TimeZone.getDefault().inDaylightTime(new Date(res));
        if (fromTsHasDayLight && !toTsHasDayLight) {
            res += TimeZone.getDefault().getDSTSavings();
        } else if (!fromTsHasDayLight && toTsHasDayLight) {
            res -= TimeZone.getDefault().getDSTSavings();
        }
        return initDate(res);
    }

    /**
     * Add Millisecond
     *
     * @param millisecond Milliseconds to Add (can be negative)
     * @param currentDate date to which days can be added
     * @return modified PersianDate
     */
    public PersianDateUTC addMillisecond(int millisecond, PersianDateUTC currentDate) {
        return currentDate.addMillisecond(millisecond);
    }

    /**
     * Compare 2 date
     *
     * @param dateInput PersianDate type
     * @return compare result
     */
    public Boolean after(PersianDateUTC dateInput) {
        return (this.timeInMilliSecond < dateInput.getTime());
    }

    /**
     * compare to data
     *
     * @param dateInput Input
     * @return compare result
     */
    public Boolean before(PersianDateUTC dateInput) {
        return (!this.after(dateInput));
    }

    /**
     * Check date equals
     *
     * @param dateInput PersianDate
     * @return equals result
     */
    public Boolean equals(PersianDateUTC dateInput) {
        return (this.timeInMilliSecond == dateInput.getTime());
    }

    /**
     * compare 2 data
     *
     * @param anotherDate PersianDate to compare
     * @return 0 = equal,1=data1 > anotherDate,-1=data1 > anotherDate
     */
    public int compareTo(PersianDateUTC anotherDate) {
        return (this.timeInMilliSecond < anotherDate.getTime() ? -1 : (this.timeInMilliSecond == anotherDate.getTime() ? 0 : 1));
    }

    /**
     * Return difference in days
     *
     * @return diff days
     */
    public long getDayUntilToday() {
        return this.getDayUntilToday(new PersianDateUTC());
    }

    /**
     * Return difference in days
     *
     * @param date date for compare
     * @return diff days
     */
    public long getDayUntilToday(PersianDateUTC date) {
        long[] ret = this.untilToday(date);
        return ret[0];
    }

    /**
     * Calc difference date until now
     *
     * @return arrays of [diffYears , diffMonths , diffDays] in orders
     */
    public long[] untilToday() {
        return this.untilToday(new PersianDateUTC());
    }

    /**
     * Calc difference between 2 date
     *
     * @param date Date 1
     * @return arrays of [diffYears , diffMonths , diffDays] in orders
     */
    public long[] untilToday(PersianDateUTC date) {
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long different = Math.abs(this.timeInMilliSecond - date.getTime());

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        long elapsedSeconds = different / secondsInMilli;
        return new long[]{elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds};
    }

    @Override
    public String toString() {
        return PersianDateUTCFormat.format(this, null);
    }

    /**
     * convert PersianDate class to date
     *
     * @return Date object of current timestamp
     */
    public Date toDate() {
        return new Date(this.timeInMilliSecond);
    }

    /**
     * Helper function for Date format
     *
     * @param number to format
     * @return formatted number
     */
    private String textNumberFilter(String number) {
        if (number.length() < 2) {
            return "0" + number;
        }
        return number;
    }

    /**
     * Helper function for Date format
     *
     * @param number to format
     * @return formatted number
     */
    private String millisecondNumberFilter(String number) {
        if (number.length() == 1) {
            return "00" + number;
        } else if (number.length() == 2) {
            return "0" + number;
        }
        return number;
    }

    /**
     * initi with time in milesecond
     */
    private void changeTime() {
        this.initDate(timeInMilliSecond);
    }

    /**
     * Return today
     *
     * @return modified PersianDate
     */
    public static PersianDateUTC today() {
        PersianDateUTC pdate = new PersianDateUTC();
        return pdate.initDateByJalali(pdate.getYear(), pdate.getMonth(), pdate.getDay(), 0, 0, 0, 0);
    }

    /**
     * Get tomorrow
     *
     * @return modified PersianDate
     */
    public static PersianDateUTC tomorrow() {
        PersianDateUTC persianDateUTC = new PersianDateUTC();
        int year = persianDateUTC.getYear();
        int month = persianDateUTC.getMonth();
        int day = persianDateUTC.getDay();
        if (persianDateUTC.getMonthLength(year, month) == day) {
            if (month == 12) {
                year++;
                month = 1;
            } else {
                month++;
            }
            day = 1;
        } else {
            day++;
        }
        return persianDateUTC.initDateByJalali(year, month, day, 0, 0, 0, 0);
    }

    /**
     * Get start of day
     *
     * @param persianDateUTC to set start of day
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfDay(PersianDateUTC persianDateUTC) {
        return new PersianDateUTC(persianDateUTC.getYear(), persianDateUTC.getMonth(), persianDateUTC.getDay(), 0, 0, 0, 0);
    }

    /**
     * Get Start of day
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfDay() {
        return this.getStartOfDay(this);
    }

    /**
     * Get end of day
     *
     * @param persianDateUTC to set end of day
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfDay(PersianDateUTC persianDateUTC) {
        return new PersianDateUTC(persianDateUTC.getYear(), persianDateUTC.getMonth(), persianDateUTC.getDay(), 23, 59, 59, 999);
    }

    /**
     * Get end of day
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfDay() {
        return this.getEndOfDay(this);
    }

    /**
     * Get start of week
     *
     * @param persianDateUTC to set start of week
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfWeek(PersianDateUTC persianDateUTC) {
        PersianDateUTC res = new PersianDateUTC(this.getYear(), this.getMonth(), this.getDay(), 0, 0, 0, 0);
        return res.addDay(persianDateUTC.dayOfWeek() * -1);
    }

    /**
     * Get start of week
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfWeek() {
        return this.getStartOfWeek(this);
    }

    /**
     * Get end of week
     *
     * @param persianDateUTC to set end of week
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfWeek(PersianDateUTC persianDateUTC) {
        PersianDateUTC res = new PersianDateUTC(this.getYear(), this.getMonth(), this.getDay(), 23, 59, 59, 999);
        return res.addDay(6 - persianDateUTC.dayOfWeek());
    }

    /**
     * Get end of week
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfWeek() {
        return this.getEndOfWeek(this);
    }

    /**
     * Get start of Month
     *
     * @param persianDateUTC to set start of month
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfMonth(PersianDateUTC persianDateUTC) {
        return new PersianDateUTC(persianDateUTC.getYear(), persianDateUTC.getMonth(), 1, 0, 0, 0, 0);
    }

    /**
     * Get start of Month
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfMonth() {
        return this.getStartOfMonth(this);
    }

    /**
     * Get end of month
     *
     * @param persianDateUTC to set end of month
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfMonth(PersianDateUTC persianDateUTC) {
        return new PersianDateUTC(persianDateUTC.getYear(), persianDateUTC.getMonth(), persianDateUTC.getMonthLength(), 23, 59, 59, 999);
    }

    /**
     * Get end of month
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfMonth() {
        return this.getEndOfMonth(this);
    }

    /**
     * Get start of Season
     *
     * @param persianDateUTC to set start of season
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfSeason(PersianDateUTC persianDateUTC) {
        int startOfSeasonMonth;
        switch (persianDateUTC.getMonth()) {
            case FARVARDIN:
            case ORDIBEHEST:
            case KHORDAD:
            default:
                startOfSeasonMonth = FARVARDIN;
                break;
            case TIR:
            case MORDAD:
            case SHAHRIVAR:
                startOfSeasonMonth = TIR;
                break;
            case MEHR:
            case ABAN:
            case AZAR:
                startOfSeasonMonth = MEHR;
                break;
            case DAY:
            case BAHMAN:
            case ESFAND:
                startOfSeasonMonth = DAY;
                break;
        }
        return new PersianDateUTC(persianDateUTC.getYear(), startOfSeasonMonth, 1, 0, 0, 0, 0);
    }

    /**
     * Get start of Season
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfSeason() {
        return this.getStartOfSeason(this);
    }

    /**
     * Get end of Season
     *
     * @param persianDateUTC to set end of season
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfSeason(PersianDateUTC persianDateUTC) {
        int endOfSeasonMonth;
        switch (persianDateUTC.getMonth()) {
            case FARVARDIN:
            case ORDIBEHEST:
            case KHORDAD:
            default:
                endOfSeasonMonth = KHORDAD;
                break;
            case TIR:
            case MORDAD:
            case SHAHRIVAR:
                endOfSeasonMonth = SHAHRIVAR;
                break;
            case MEHR:
            case ABAN:
            case AZAR:
                endOfSeasonMonth = AZAR;
                break;
            case DAY:
            case BAHMAN:
            case ESFAND:
                endOfSeasonMonth = ESFAND;
                break;
        }
        return new PersianDateUTC(persianDateUTC.getYear(), endOfSeasonMonth, persianDateUTC.getMonthLength(persianDateUTC.getYear(), endOfSeasonMonth), 23, 59, 59, 999);
    }

    /**
     * Get end of Season
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfSeason() {
        return this.getEndOfSeason(this);
    }

    /**
     * Get start of Year
     *
     * @param persianDateUTC to set start of year
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfYear(PersianDateUTC persianDateUTC) {
        return new PersianDateUTC(persianDateUTC.getYear(), FARVARDIN, 1, 0, 0, 0, 0);
    }

    /**
     * Get start of Year
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getStartOfYear() {
        return this.getStartOfYear(this);
    }

    /**
     * Get end of Year
     *
     * @param persianDateUTC to set end of year
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfYear(PersianDateUTC persianDateUTC) {
        return new PersianDateUTC(persianDateUTC.getYear(), ESFAND, persianDateUTC.getMonthLength(persianDateUTC.getYear(), ESFAND), 23, 59, 59, 999);
    }

    /**
     * Get end of Year
     *
     * @return modified PersianDate
     */
    public PersianDateUTC getEndOfYear() {
        return this.getEndOfYear(this);
    }

    /**
     * Get Day Range
     *
     * @param persianDateUTC to get Day Range
     * @return Pair of start and end of day timestamp
     */
    public Pair<Long, Long> getDayRange(PersianDateUTC persianDateUTC) {
        return new Pair<Long, Long>(persianDateUTC.getStartOfDay().getTime(), persianDateUTC.getEndOfDay().getTime());
    }

    /**
     * Get Day Range
     *
     * @return Pair of start and end of day timestamp
     */
    public Pair<Long, Long> getDayRange() {
        return this.getDayRange(this);
    }

    /**
     * Get Week Range
     *
     * @param persianDateUTC to get Week Range
     * @return Pair of start and end of Week timestamp
     */
    public Pair<Long, Long> getWeekRange(PersianDateUTC persianDateUTC) {
        return new Pair<Long, Long>(persianDateUTC.getStartOfWeek().getTime(), persianDateUTC.getEndOfWeek().getTime());
    }

    /**
     * Get Week Range
     *
     * @return Pair of start and end of Week timestamp
     */
    public Pair<Long, Long> getWeekRange() {
        return this.getWeekRange(this);
    }

    /**
     * Get Month Range
     *
     * @param persianDateUTC to get Month Range
     * @return Pair of start and end of Month timestamp
     */
    public Pair<Long, Long> getMonthRange(PersianDateUTC persianDateUTC) {
        return new Pair<Long, Long>(persianDateUTC.getStartOfMonth().getTime(), persianDateUTC.getEndOfMonth().getTime());
    }

    /**
     * Get Month Range
     *
     * @return Pair of start and end of Month timestamp
     */
    public Pair<Long, Long> getMonthRange() {
        return this.getMonthRange(this);
    }

    /**
     * Get Season Range
     *
     * @param persianDateUTC to get Season Range
     * @return Pair of start and end of Season timestamp
     */
    public Pair<Long, Long> getSeasonRange(PersianDateUTC persianDateUTC) {
        return new Pair<Long, Long>(persianDateUTC.getStartOfSeason().getTime(), persianDateUTC.getEndOfSeason().getTime());
    }

    /**
     * Get Season Range
     *
     * @return Pair of start and end of Season timestamp
     */
    public Pair<Long, Long> getSeasonRange() {
        return this.getSeasonRange(this);
    }

    /**
     * Get Year Range
     *
     * @param persianDateUTC to get Year Range
     * @return Pair of start and end of Year timestamp
     */
    public Pair<Long, Long> getYearRange(PersianDateUTC persianDateUTC) {
        return new Pair<Long, Long>(persianDateUTC.getStartOfYear().getTime(), persianDateUTC.getEndOfYear().getTime());
    }

    /**
     * Get Year Range
     *
     * @return Pair of start and end of Year timestamp
     */
    public Pair<Long, Long> getYearRange() {
        return this.getYearRange(this);
    }

    /**
     * Check midnight
     *
     * @param persianDateUTC to check midnight
     * @return is midnight result
     */
    public Boolean isMidNight(PersianDateUTC persianDateUTC) {
        return persianDateUTC.isMidNight();
    }

    /**
     * Check is midNight
     *
     * @return is midnight result
     */
    public Boolean isMidNight() {
        return (this.hour < 12);
    }

    /**
     * Get short name time of the day
     *
     * @return short name time of the day
     */
    public String getShortTimeOfTheDay() {
        return (this.isMidNight()) ? AM_SHORT_NAME : PM_SHORT_NAME;
    }

    /**
     * Get short name time of the day
     *
     * @return short name time of the day
     */
    public String getShortTimeOfTheDay(PersianDateUTC persianDateUTC) {
        return (persianDateUTC.isMidNight()) ? AM_SHORT_NAME : PM_SHORT_NAME;
    }

    /**
     * Get time of the day
     *
     * @return time of the day
     */
    public String getTimeOfTheDay() {
        return (this.isMidNight()) ? AM_NAME : PM_NAME;
    }

    /**
     * Get time of the day
     *
     * @return time of the day
     */
    public String getTimeOfTheDay(PersianDateUTC persianDateUTC) {
        return (persianDateUTC.isMidNight()) ? AM_NAME : PM_NAME;
    }

    /**
     * Get number of days in month
     *
     * @param year  Jalali year
     * @param month Jalali month
     * @return number of days in month
     */
    public Integer getMonthLength(Integer year, Integer month) {
        if (month <= 6) {
            return 31;
        } else if (month <= 11) {
            return 30;
        } else {
            if (this.isLeap(year)) {
                return 30;
            } else {
                return 29;
            }
        }
    }

    /**
     * Get number of days in month
     *
     * @param persianDateUTC persianDate object
     * @return number of days in month
     */
    public Integer getMonthLength(PersianDateUTC persianDateUTC) {
        return this.getMonthLength(persianDateUTC.getYear(), persianDateUTC.getMonth());
    }

    /**
     * Get number of days in month
     *
     * @return number of days in month
     */
    public Integer getMonthLength() {
        return this.getMonthLength(this);
    }

    /**
     * remove millisecond from timestamp
     *
     * @param date timestamp
     * @return millisecond removed timestamp
     */
    public static long removeMilliSecond(long date) {
        return (date - (date % 1000));
    }

    private int validateMonth(int month) {
        if (month > 12) {
            return 12;
        } else return Math.max(month, 1);
    }

    private int validateDay(int day) {
        if (day > getMonthLength()) {
            return getMonthLength();
        } else {
            return Math.max(day, 1);
        }
    }

    private int validateHour(int hour) {
        if (hour > 23) {
            return 0;
        } else {
            return Math.max(hour, 0);
        }
    }

    private int validateMinuteAndSecond(int minuteOrSecond) {
        if (minuteOrSecond > 59) {
            return 0;
        } else {
            return Math.max(minuteOrSecond, 0);
        }
    }

    private int validateMillisecond(int millisecond) {
        if (millisecond > 999) {
            return 0;
        } else {
            return Math.max(millisecond, 0);
        }
    }
}